melody = [1,4,7,8,9]
scale = [Scale.iwato, Scale.jiao]

fm.set degree: melody.p(0.5),
       gate: :degree,
       detune: [0.2, 0.1],
       sustain: [5,3,10],
       accelerate: [0, 0.1],
       octave: [3,3,2,4].p(1/3),
       scale: scale.p(1/2) 

kick.set freq: s("xi.x .ix. | xi.x xx.x", 70, 200), amp: 0.9, gate: :freq

clap.set n: s("..x. xyz. .x.. .xyx", 60, 61, 60).p.decelerate(2),
         gate: :n,
         amp: 0.1,
         pan: P.sin(16, 2) * 0.6,
         sustain: 0.25

shat.set freq: s("xi.x .ix. | xi.x xx.x", 70, 200), amp: 0.8, gate: :freq
